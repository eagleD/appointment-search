<?php
namespace App\Repository;

use Illuminate\Support\Collection;
use App\Models\Calendar;
use App\Models\TimeslotType;

interface BookingRepoInterface 
{
  /**
   * Get all data from Json files.
   *
   * @param array $relations
   * @return Collection
   */
  public function getSourceData();
  
  public function getAllCalendars(): Collection;

  /**
   * Find Calendar by id.
   *
   * @param string $id
   * @return Calendar
   */
  public function findCalendarById(string $id): ?Calendar;
  
  /**
   * Get all Timeslots.
   *
   * @param array $relations
   * @return Collection
   */
  public function getAllTimeslots(array $relations = []): Collection;

  /**
   * Get all available Timeslots.
   *
   * @param array $relations
   * @return Collection
   */
  public function getAllAvailableTimeslots(array $relations = []): Collection;

  /**
   * Get all Appointments.
   *
   * @param array $relations
   * @return Collection
   */
  public function getAllAppointments(array $relations = []): Collection;

  /**
   * Get all TimeslotTypes.
   *
   * @return Collection
   */
  public function getAllTimeslotTypes(): Collection;

  /**
   * Find TimeslotType by id.
   *
   * @param string $id
   * @return Calendar
   */
  public function findTimeslotTypeById(string $id): ?TimeslotType;
  
}