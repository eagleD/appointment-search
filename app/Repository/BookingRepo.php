<?php

namespace App\Repository;

use Illuminate\Support\Facades\Storage; 
use \Carbon\Carbon;
use Illuminate\Support\Collection;
use App\Models\Calendar;
use App\Models\TimeslotType;
use File;

class BookingRepo implements BookingRepoInterface
{

  /**
   * @return array
   */
  public function getSourceData() 
  {
    $path = database_path('data');
    $files = File::files($path);

    return collect($files)->map(function($file) {
      $json = File::get($file);
      $data = json_decode($json);
      return $data;
    });
  }

  /**
   * @return Collection
   */
  public function getAllCalendars(): Collection 
  {
    return collect([
      ['id' => "48644c7a-975e-11e5-a090-c8e0eb18c1e9", 'name' => "Joanna Hef"],
      ['id' => "48cadf26-975e-11e5-b9c2-c8e0eb18c1e9", 'name' => "Danny Boy"],
      ['id' => "452dccfc-975e-11e5-bfa5-c8e0eb18c1e9", 'name' => "Emma Win"],
    ]);
  }

  /**
   * @param string $id
   * @return Calendar
   */
  public function findCalendarById(string $id): ?Calendar
  { 
    $result = $this->getAllCalendars()->firstWhere('id', $id);
    if ($result){
      $data = (object) $result;
      return new Calendar($data->id, $data->name);
    }
    else 
      return NULL;
  }

  /**
   * @param array $relations
   * @return Collection
   */
  public function getAllTimeslots(array $relations = []): Collection
  {
    $timeslots = collect([]);    
    foreach($this->getSourceData() as $data) {  
        if (property_exists($data, 'timeslots')) {
            $curr = collect($data->timeslots)->map(function($slot) use ($relations) {
                $start = Carbon::parse($slot->start);
                $end = Carbon::parse($slot->end);
                $slot->duration_in_minutes = $start->diffInMinutes($end);   
                
                if (in_array('type', $relations)) {
                  $slot->type = $this->findTimeslotTypeById($slot->type_id);
                }
                if (in_array('calendar', $relations)) {
                  $slot->calendar = $this->findCalendarById($slot->calendar_id);
                }
                return $slot;
            });
            $timeslots = $timeslots->merge($curr);
        }
    }
    return $timeslots->sortBy('start');
  }

  /**
   * @param array $relations
   * @return Collection
   */
  public function getAllAvailableTimeslots(array $relations = []): Collection 
  {
    return $this->getAllTimeslots($relations)->where('public_bookable', true)->where('out_of_office', false);
  }

  /**
   * @param array $relations
   * @return Collection
   */
  public function getAllAppointments(array $relations = []): Collection
  { 
    $appointments = collect([]);
    foreach($this->getSourceData() as $data) {
      if (property_exists($data, 'appointments')) {
        $rows = collect($data->appointments)->map(function($appointment) use ($relations){  
          if (in_array('timeslot_type', $relations)) {
            $appointment->time_slot_type = $this->findTimeslotTypeById($appointment->time_slot_type_id);
          }
          return $appointment;
        });
        $appointments = $appointments->merge($rows);
      }
    }
    return $appointments;
  }

  /**
   * @return Collection
   */
  public function getAllTimeslotTypes(): Collection
  {
    $types = collect([]);
    foreach($this->getSourceData() as $data) {
      if (property_exists($data, 'timeslottypes')) {
        $types = $types->merge($data->timeslottypes);
      }
    }
    return $types;
  }

  /**
   * @param string $id
   * @return TimeslotType
   */
  public function findTimeslotTypeById(string $id): ?TimeslotType
  {
    $data = $this->getAllTimeslotTypes()->firstWhere('id', $id);
    if ($data)
      return new TimeslotType($data->id, $data->name, $data->slot_size);
    else 
      return NULL;
  }

  /**
   * @param array $calendarIds calendar ids
   * @param int $duration duration of the meeting in minutes 
   * @param string $start start date  
   * @param string $end end date  
   * @param string $typeId timeslot type id
   * 
   * @return TimeslotType
   */
  public function findAvailableTime(array $calendarIds=[], $duration=NULL, $start=NULL, $end=NULL, $typeId=NULL)
  {
    $calendarIds = array_filter($calendarIds);
    $startParse = Carbon::parse($start);
    $endParse = Carbon::parse($end);

    // format dates to ISO8601
    $startDate = $startParse->format('c');
    // get end of the day if time is not specified
    $endDate = $endParse->lessThanOrEqualTo($startParse) ? 
            $endParse->endOfDay()->format('c') : 
            $endParse->format('c');

    // apply the filters
    $timeslots = $this->getAllAvailableTimeslots(['type'])
              ->when($calendarIds, function ($query, $calendarIds) {
                return $query->whereIn('calendar_id', $calendarIds);
              })
              ->when($startDate, function ($query, $startDate) {
                return $query->where('start', ">=", $startDate);
              })
              ->when($endDate, function ($query, $endDate) {
                return $query->where('end', "<=", $endDate);
              })
              ->when($typeId, function($query, $typeId) {
                return $query->where('type.id', $typeId);
              });
    
    // get all the appointments within the date period specified
    $appointments = $this->getAllAppointments()
                      ->when($startDate, function ($query, $startDate) {
                        return $query->where('start', ">=", $startDate);
                      })
                      ->when($endDate, function ($query, $endDate) {
                        return $query->where('end', "<=", $endDate);
                      })
                      ->when($calendarIds, function ($query, $calendarIds) {
                        return $query->whereIn('calendar_id', $calendarIds);
                      });

    // match the appointments with the timeslots 
    // get only the timeslots with no appointments
    $timeslots = $timeslots->filter(function ($slot) use ($appointments) {
      // check appointments per calendar
      $count = $appointments->where('calendar_id', $slot->calendar_id)
                      ->filter(function ($appt) use ($slot){
                          $slotStart = Carbon::parse($slot->start);
                          $slotEnd = Carbon::parse($slot->end);
                          $apptStart = Carbon::parse($appt->start);
                          $apptEnd = Carbon::parse($appt->end);
                          return $apptStart->between($slotStart, $slotEnd) ||
                                  $apptEnd->betweenExcluded($slotStart, $slotEnd);
                      })->count();
      // TODO: for the optional parameter type, we can check the slot_size if we can still open the slot for appointments
      return $count <= 0;      
    });

    // filter duration 
    if ($duration) {
      // check for exact match
      $combined = $timeslots->filter(function($slot) use ($duration) {
        return $slot->duration_in_minutes >= $duration;
      }); 

      // check per calendar for adjacent timeslots
      $calendars = $timeslots->groupBy('calendar_id');
      foreach ($calendars as $calId => $slots) {
        // group adjacent timeslots per calendar 
        $timeGroups = collect([]);
        $subGroup = collect([]);
        $end = NULL;
        foreach($slots as $key => $slot) {
          if (!$end || $end == $slot->start) {
            $subGroup->push($slot);
            $end = $slot->end;

            if ($key == count($slots) - 1) {
              $timeGroups->push($subGroup);
            }
          } else {
            $timeGroups->push($subGroup);
            $tempGroup = collect([]);
          }
        }
        
        // filter for adjacent timeslots group  
        $adjacentMatches = collect([]);
        foreach($timeGroups as $group) {
          $group = $group->sortBy('start');     

          // get the suppose date range base on duration     
          $fromGroupTime = Carbon::parse($group->first()->start);
          $durationEndTime = $fromGroupTime->copy()->addMinutes($duration);

          // filter the group that falls within the suppose time range
          $group = $group->filter(function($g) use($fromGroupTime, $durationEndTime){
            return Carbon::parse($g->start)->between($fromGroupTime, $durationEndTime) && 
                    (Carbon::parse($g->end)->between($fromGroupTime, $durationEndTime) ||
                    $durationEndTime->lessThanOrEqualTo(Carbon::parse($g->end)));
                    
          });  
          // make sure the duration for the entire group is within the desired duration
          if ($group->sum('duration_in_minutes') >= $duration) {
            $adjacentMatches = $adjacentMatches->merge($group);
          }
        }

        $combined = $combined->merge($adjacentMatches);
      }
      $timeslots = $combined->unique();
    }

    return $timeslots->sortBy([
      ['calendar_id', 'asc'],
      ['start', 'asc']
    ]);
  }

}