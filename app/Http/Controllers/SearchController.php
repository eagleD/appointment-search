<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\BookingRepoInterface; 
use \Carbon\Carbon;

class SearchController extends Controller
{
    private $bookingRepo; 

    public function __construct(BookingRepoInterface $bookingRepo) 
    {
        $this->bookingRepo = $bookingRepo;
    }

    public function index(Request $request)
    {
        // $calendarIds = [
        //     "48644c7a-975e-11e5-a090-c8e0eb18c1e9", 
        //     "48cadf26-975e-11e5-b9c2-c8e0eb18c1e9",
        //     "452dccfc-975e-11e5-bfa5-c8e0eb18c1e9"
        // ];
        
        // sample query inputs
        $calendarIds = explode(',', $request->get('calendarIds'));
        $start = $request->get('start', "2019-04-23T08:00:00");
        $end = $request->get('end', "2019-04-23T09:30:00");
        $durationInMinutes = $request->get('duration', 60);
        $typeId = $request->get('typeId', "4529821e-975e-11e5-bbaf-c8e0eb18c1e9"); // 452935de-975e-11e5-ae1a-c8e0eb18c1e9
        
        $data = $this->bookingRepo->findAvailableTime($calendarIds, $durationInMinutes, $start, $end, $typeId);
        
        return response()->json([
            'count' => $data->count(),
            'data' => $data->values()->toArray()
        ]);
    }

}
