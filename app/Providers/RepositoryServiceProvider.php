<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repository\BookingRepoInterface; 
use App\Repository\BookingRepo; 

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(BookingRepoInterface::class, BookingRepo::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
