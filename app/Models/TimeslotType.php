<?php

namespace App\Models;

class TimeslotType
{
  public $id; 
  public $name;
  public $slot_size;

  public function __construct($id, $name, $slot_size) {
    $this->id = $id;
    $this->name = $name;
    $this->slot_size = $slot_size;
  }

  
}